var express  = require("express"),
    app      = express(),
    http     = require("http"),
    server   = http.createServer(app),
    mongoose = require('mongoose'),
    cors     = require('cors');

  app.use(cors());

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

routes = require('./routes/peliculas')(app);

app.get('/', function(req, res) {
  res.send("hola");
});

mongoose.connect("mongodb://localhost:27017/trabajo")


server.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});
