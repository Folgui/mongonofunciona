var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var PeliculaSchema = new Schema({
	titulo: 			{ type: String },
	director: 			{ type: String },
	duracion: 			{ type: String }, 
	sinopsis: 			{ type: String },
	fechaLanzamiento: 	{ type: String }
});

module.exports = mongoose.model('Films', PeliculaSchema,"Peliculas");
