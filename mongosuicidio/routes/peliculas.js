//File: routes/tvshows.js
module.exports = function(app) {

  var Films = require('../models/peliculas.js');

  todasPeliculas = function(req, res) {
    Films.find(function(err, respuesta) {
  		if(!err) {
        console.log('GET /peliculas');
  			res.send(respuesta);
  		} else {
  			console.log('ERROR: ' + err);
  		}
  	});
  };
  tituloPelicula = function(req, res) {
    Films.find({titulo: req.params.titulo}, function(err, respuesta) {
    	if(!err) {
        console.log('GET /peliculas/' + req.params.titulo);
    		res.send(respuesta);
    	} else {
    		console.log('ERROR: ' + err);
    	}
    });
  };

  addPelicula = function(req, res) {
    console.log('POST');
    console.log(req.body.fechaLanzamiento);

    var Pelicula = new Films({
  		titulo: 			req.body.titulo,
  		director: 			req.body.director,
  		duracion: 			req.body.duracion,
  		sinopsis: 			req.body.sinopsis,
  		fechaLanzamiento: 	req.body.fechaLanzamiento
  	});

    Pelicula.save(function(err) {
      if(!err) {
        console.log('Created');
      } else {
        console.log('ERROR: ' + err);
      }
    });
    res.send(Pelicula);
  };

    delPelicula = function(req, res){
        Films.findOneAndRemove({titulo: req.params.titulo}, (err, respuesta) => {
            if(!err) {
            console.log(respuesta.titulo + ' fue borrada');
            } else {
            console.log('ERROR: ' + err);
            }
            res.send(respuesta);
        });
    }

  app.get('/peliculas', todasPeliculas);
  app.get('/peliculas/:titulo', tituloPelicula);
  app.post('/peliculas/add', addPelicula);
  app.delete('/peliculas/del/:titulo', delPelicula);


}
