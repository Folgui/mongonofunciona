import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root',
})
export class apiServicios {

    pelisUrl: string = 'http://localhost:3000/peliculas';

    constructor(private http: HttpClient){}

    private gettings(res: Response) {
            let body = res;
            return body;
    };

    getPeliculas(): Observable<any> {
        return this.http.get(this.pelisUrl, httpOptions).pipe(
            map(this.gettings)
        )
    }

    getPelicula(titulo): Observable<any> {
        return this.http.get(this.pelisUrl + '/' + titulo, httpOptions).pipe(
            map(this.gettings)
        )
    }

    addPelicula(pelicula: any) {
        let headers = new Headers;
        let body = JSON.stringify({

                titulo: pelicula.titulo, 
                director: pelicula.director, 
                duracion: pelicula.duracion, 
                fechaLanzamiento: pelicula.fechaSalida, 
                sinopsis: pelicula.sinopsis

        });

        headers.append('Content-Type', 'application/json');
        console.log(body);
        return this.http.post(this.pelisUrl + '/add', body, httpOptions).pipe(
            map(this.gettings)
        ); 
    }

    delPelicula(titulo){
        return this.http.delete(this.pelisUrl + '/del/' + titulo, httpOptions).pipe(
            map(this.gettings)
        );
    }

}
