import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { InicioComponent } from './Inicio/inicio.component';
import { PeliculasComponent } from './Peliculas/peliculas.component';
import { CriticosComponent } from './Criticos/criticos.component';
import { CrudPeliculasComponent } from './CRUD.Peliculas/crud.peliculas.component';

const appRoutes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'Inicio', component: InicioComponent },
  { path: 'Peliculas', component: PeliculasComponent },
  { path: 'Criticos', component: CriticosComponent },
  { path: 'addPelicula', component: CrudPeliculasComponent },
  { path: '**', component: InicioComponent }
];


export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
