import { Component, OnInit } from '@angular/core';

import { apiServicios } from '../servicios/api.servicios';

@Component({
  selector: 'app-crud.peliculas',
  templateUrl: './crud.peliculas.component.html',
  styleUrls: ['./crud.peliculas.component.css']
})
export class CrudPeliculasComponent implements OnInit {

	pelicula: any = {
		titulo: '',
		director: '',
		duracion: '',
		fechaSalida: '',
		sinopsis: ''
	}

	constructor(public api: apiServicios) { }

	ngOnInit() {

	}

	public onsubmit(myForm){
		this.api.addPelicula(this.pelicula).subscribe(res => {
				console.log("Pelicula Agregada");
			},
		);
		myForm.reset();
	}

}
