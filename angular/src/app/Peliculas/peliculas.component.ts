import { Component, OnInit} from '@angular/core';

import { apiServicios } from '../servicios/api.servicios';

import * as $ from 'jquery';

@Component({
  selector: 'peliculas',
  templateUrl:'./peliculas.component.html',
  styleUrls:['./peliculas.component.css'],
})

export class PeliculasComponent implements OnInit{

  peliis: any;

  particular: any;

  constructor(private api: apiServicios ){ }

  ngOnInit(){

    this.api.getPeliculas()
      .subscribe(res => {
        console.log(res);
        this.peliis = res;
        }, err => {
            console.log(err);
        });

  }

  selectPelicula(titulo){
    this.api.getPelicula(titulo)
      .subscribe(res => {
        console.log(res);
        this.particular = res;
        return this.particular;
      }, err => {
          console.log(err);
      });
  }

  delPelicula(titulo){
    this.api.delPelicula(titulo)
      .subscribe(res => { 
        console.log("Borrada");
      }, err => {
            console.log(err);
        });
  }

}
