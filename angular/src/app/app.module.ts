import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { routing, appRoutingProviders } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { InicioComponent } from './Inicio/inicio.component';
import { PeliculasComponent } from './Peliculas/peliculas.component';
import { CriticosComponent } from './Criticos/criticos.component';
import { CrudPeliculasComponent } from './CRUD.Peliculas/crud.peliculas.component';
import { NavegadorComponent } from './Navegador/navegador.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    PeliculasComponent,
    CriticosComponent,
    CrudPeliculasComponent,
    NavegadorComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
