import {Component, OnInit} from '@angular/core';
@Component({
  selector: 'inicio',
  templateUrl:'./inicio.component.html',
  styleUrls:['./inicio.component.css'],
})

export class InicioComponent{

  public titulo: string;

  constructor(){
    this.titulo = "CriticAhs...";
  }

}
